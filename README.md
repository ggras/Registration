# Registration Project

This project contains two algorithms for hand-eye calibration.

### Prerequisites

Erl.

Eigen 3.3.

A compiler with C++11 support (MSVC 14.0 or higher on Windows).

### Installing

The templated function are provided in a .cpp file, with a corresponding header file allowing explicit instantiation through compilation. This is mainly useful to avoid long compilation times due to the use of the Eigen svd function. The templates can of course also be used in a header-only fashion if desired.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

