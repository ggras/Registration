/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#ifndef ERL_HANDEYE_H
#define ERL_HANDEYE_H

#include "Erl/_platform/_exports.h"
#include <Erl.h>

namespace Erl
{

template <typename Type> _ERL_EXPORT Erl::Transform<Type> AXXB (Erl::Transform<Type> *A, Erl::Transform<Type> *B, int numMotions);
template <typename Type> _ERL_EXPORT Erl::Transform<Type> AXXB_IDQ (Erl::Transform<Type> *A, Erl::Transform<Type> *B, int numMotions);

}

#endif
