/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#include <Erl.h>
#include <handeye.h>
#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    srand(time(NULL));

    // Generate random values for testing purposes.

    Erl::Transformd L = Erl::Transformd::Random(400.0);
    Erl::Transformd X = Erl::Transformd::Random(20.0);

    int numSamples = 500;
    std::vector<Erl::Transformd> T_alpha(numSamples);
    std::vector<Erl::Transformd> T_beta(numSamples);

    Erl::Transformd T0 = Erl::Transformd::Random(200.0);
    T_alpha[0] = T0;
    T_beta[0]  = L.inv() * T_alpha[0] * X;

    for (int i = 1; i < numSamples; i++)
    {
        // The solver works best with small changes between steps.
        // Purely random T_alpha values would have resulted in very large changes between steps, making the solver fail.

        T_alpha[i] = T_alpha[i-1] * Erl::Transformd(Erl::Rotmatd::fromAngleAxis(Erl::toRadian(1.0), Erl::Vector3d::Random()), Erl::Vector3d::Random());
        T_beta[i]  = L.inv() * T_alpha[i] * X;
    }

    // We have our generated T_alpha and T_beta values, the real algorithm begins here.

    std::vector<Erl::Transformd> A(T_alpha.size() - 1);
    std::vector<Erl::Transformd> B(T_alpha.size() - 1);
    for (unsigned i = 0; i < A.size(); i++)
    {
        A[i] = T_alpha[i].inv() * T_alpha[i+1];
        B[i] = T_beta [i].inv() * T_beta [i+1];
    }
    Erl::Transformd X_calc = Erl::AXXB_IDQ(A.data(), B.data(), A.size());

    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> A_st;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> B_st;
    A_st.resize(4 * numSamples, 4);
    B_st.resize(4 * numSamples, 4);

    for (int i = 0; i < numSamples; i++)
    {
        Erl::Transformd As = X_calc.inv() * T_alpha[i].inv();
        A_st.block<4,4>(i*4, 0) = As.getEigenMatrix();
        B_st.block<4,4>(i*4, 0) = T_beta[i].inv().getEigenMatrix();
    }
    Erl::Transformd L_calc = Erl::Transformd::fromEigenMatrix(A_st.colPivHouseholderQr().solve(B_st));

    cout << "Real X value: \n" << X << endl;
    cout << "Computed X value: \n" << X_calc << endl;
    cout << "X value difference: \n" << X.inv() * X_calc << endl;

    cout << "\n" << endl;

    cout << "Real L value: \n" << L << endl;
    cout << "Computed L value: \n" << L_calc << endl;
    cout << "L value difference: \n" << L.inv() * L_calc << endl;

    return 0;
}

