/*
 * Copyright (c) 2013-2017 Gauthier Gras <gauthier.gras@gmail.com>
 * Licensed under the MIT license. See the license file LICENSE.
*/

#include "handeye.h"
#include <Eigen/Dense>

namespace Erl
{

/// Implemented from: Daniilidis, Konstantinos. "Hand-eye calibration using dual quaternions."
/// The International Journal of Robotics Research 18.3 (1999): 286-298.
template <typename Type> Transform<Type> AXXB (Transform<Type> *A, Transform<Type> *B, int numMotions)
{
    using MatXt = Eigen::Matrix<Type, Eigen::Dynamic, Eigen::Dynamic>;
    using VecXt = Eigen::Matrix<Type, Eigen::Dynamic, 1>;

    if (numMotions < 2)
    {
        std::cout << "\nAXXB: Two motions minimum are required" << std::endl;
        return Transform<Type>();
    }
    MatXt T;

    for (int i = 0; i < numMotions; i++)
    {
        DualQuaternion<Type> a = A[i];
        DualQuaternion<Type> b = B[i];
        Vector3<Type> a1 = a.Qr().getVector();
        Vector3<Type> a2 = a.Qd().getVector();
        Vector3<Type> b1 = b.Qr().getVector();
        Vector3<Type> b2 = b.Qd().getVector();

        MatXt S(6,8);
        Rotmat<Type> X, X1;
        X << 0, -(a1+b1)[2], (a1+b1)[1],
                (a1+b1)[2], 0, -(a1+b1)[0],
                -(a1+b1)[1], (a1+b1)[0], 0;
        X1 << 0, -(a2+b2)[2], (a2+b2)[1],
                (a2+b2)[2], 0, -(a2+b2)[0],
                -(a2+b2)[1], (a2+b2)[0], 0;
        S << a1 - b1, X, Vector3<Type>::Zero(), Rotmat<Type>::Zero(),
                a2 - b2, X1, a1 - b1, X;
        if (i == 0)
            T = S;
        else
        {
            MatXt temp = T;
            T.resize(6*(i+1),8);
            T << temp, S;
        }
    }

    Eigen::JacobiSVD<MatXt> svd (T, Eigen::ComputeFullU | Eigen::ComputeFullV);
    MatXt V = svd.matrixV ();
    VecXt v7 = V.col(6);
    VecXt v8 = V.col(7);
    Eigen::Matrix<Type, 4, 1> u1 = v7.head(4);
    Eigen::Matrix<Type, 4, 1> v1 = v7.tail(4);
    Eigen::Matrix<Type, 4, 1> u2 = v8.head(4);
    Eigen::Matrix<Type, 4, 1> v2 = v8.tail(4);

    Type c1 = u1.transpose()*v1;
    Type c2 = (Type)(u1.transpose()*v2) + (Type)(u2.transpose()*v1);
    Type c3 = u2.transpose()*v2;
    Type delta = c2*c2 - 4*c1*c3;
    if (delta < 0 || c1 == 0)
    {
        std::cout << "\nProblem while computing s from Null space vectors: c1 = " << c1 << ", delta = " << delta << std::endl;
        return Transform<Type>();
    }
    Type s1 = (- c2 - sqrt(delta))/(2*c1);
    Type s2 = (- c2 + sqrt(delta))/(2*c1);

    Type toD, tempd, L1, L2;
    Type s = s1;
    toD = s1*s1*(Type)(u1.transpose()*u1) + 2*s1*(Type)(u1.transpose()*u2) + (Type)(u2.transpose()*u2);
    tempd = s2*s2*(Type)(u1.transpose()*u1) + 2*s2*(Type)(u1.transpose()*u2) + (Type)(u2.transpose()*u2);
    if (tempd > toD)
    {
        toD = tempd;
        s = s2;
    }
    if (toD == 0)
    {
        std::cout << "\nProblem while computing s from Null space vectors: toD = " << toD << ", s = " << s << std::endl;
        return Transform<Type>();
    }
    L2 = sqrt(1/toD);
    L1 = s*L2;
    Quaternion<Type> q1(L1*u1[0] + L2*u2[0], L1*u1[1] + L2*u2[1], L1*u1[2] + L2*u2[2], L1*u1[3] + L2*u2[3]);
    Quaternion<Type> q2(L1*v1[0] + L2*v2[0], L1*v1[1] + L2*v2[1], L1*v1[2] + L2*v2[2], L1*v1[3] + L2*v2[3]);

    DualQuaternion<Type> Qresult(q1, q2);
    return Qresult.template getTransform<ColMajor>();
}

/// Implemented from: Malti, Abed, and Joao P. Barreto. "Robust hand-eye calibration for computer aided medical endoscopy."
/// Robotics and Automation (ICRA), 2010 IEEE International Conference on. IEEE, 2010.
template <typename Type> Transform<Type> AXXB_IDQ (Transform<Type> *A, Transform<Type> *B, int numMotions)
{
    using MatXt = Eigen::Matrix<Type, Eigen::Dynamic, Eigen::Dynamic>;
    using VecXt = Eigen::Matrix<Type, Eigen::Dynamic, 1>;

    if (numMotions < 2)
    {
        std::cout << std::endl << "AXXB: Two motions minimum are required" << std::endl;
        return Transform<Type>();
    }
    MatXt L, L1;
    Quaternion<Type> q1, q2;

    for (int i = 0; i < numMotions; i++)
    {
        DualQuaternion<Type> a = A[i];
        DualQuaternion<Type> b = B[i];
        Vector3<Type> a1 = a.Qr().getVector();
        Vector3<Type> a2 = a.Qd().getVector();
        Vector3<Type> b1 = b.Qr().getVector();
        Vector3<Type> b2 = b.Qd().getVector();
        Type a0 = a.Qr().w();
        Type b0 = b.Qr().w();
        Type a01 = a.Qd().w();
        Type b01 = b.Qd().w();

        MatXt S(4,4);
        MatXt S1(4,4);
        Rotmat<Type> X, X1;
        Vector3<Type> c = a1 + b1;
        X << 0, -c[2], c[1],
                c[2], 0, -c[0],
                -c[1], c[0], 0;
        Vector3<Type> c1 = a2 + b2;
        X1 << 0, -c1[2], c1[1],
                c1[2], 0, -c1[0],
                -c1[1], c1[0], 0;
        S << a0 - b0, -(a1 - b1).transpose(),
                a1 - b1, X + (a0 - b0)*Rotmat<Type>::Identity();
        S1 << a01 - b01, -(a2 - b2).transpose(),
                a2 - b2, X1 + (a01 - b01)*Rotmat<Type>::Identity();
        if (i == 0)
        {
            L = S;
            L1 = S1;
        }
        else
        {
            MatXt temp = L;
            L.resize(4*(i+1),4);
            L << temp, S;
            MatXt temp1 = L1;
            L1.resize(4*(i+1),4);
            L1 << temp1, S1;
        }
    }

    Eigen::JacobiSVD<MatXt> svd (L, Eigen::ComputeFullU | Eigen::ComputeFullV);
    MatXt V = svd.matrixV ();
    VecXt v4 = V.col(3);
    q1 = Quaternion<Type>(v4[0], v4[1], v4[2], v4[3]);

    MatXt b = -L1*v4;
    Eigen::HouseholderQR<MatXt> qr(v4);
    MatXt R = qr.matrixQR().template triangularView<Eigen::Upper>();
    MatXt Q = qr.householderQ();
    MatXt Atemp = L*Q;
    MatXt A2 = Atemp.block(0, R.cols(), Atemp.rows(), Atemp.cols() - 1);
    Eigen::JacobiSVD<MatXt> lsqtlin (A2, Eigen::ComputeFullU | Eigen::ComputeFullV);
    VecXt z = lsqtlin.solve(b);
    VecXt Vtemp(4);
    Vtemp << 0, z;
    VecXt x = Q*Vtemp;
    q2 = Quaternion<Type>(x[0], x[1], x[2], x[3]);

    DualQuaternion<Type> Qresult(q1, q2);
    return Qresult.template getTransform<ColMajor>();
}

template Erl::Transform<double> AXXB<double>(Erl::Transform<double> *, Erl::Transform<double> *, int);
template Erl::Transform<float>  AXXB<float>(Erl::Transform<float> *, Erl::Transform<float> *, int);
template Erl::Transform<double> AXXB_IDQ<double>(Erl::Transform<double> *, Erl::Transform<double> *, int);
template Erl::Transform<float>  AXXB_IDQ<float>(Erl::Transform<float> *, Erl::Transform<float> *, int);

}
